class customers {
    constructor(arr) {
        this.obj = arr.reduce((obj, item) => {
            obj[item] = []
            return obj
        }, {})
    }

    addItemToCart(name, item) {
        this.obj[name] = [...this.obj[name], { itemName: item, purchased: false }]
    }

    removeItemFromCart(name, item) {
        this.obj[name] = this.obj[name].filter(eachItem => {
            if (eachItem.itemName !== item) {
                return eachItem
            }
        })

    }

    purchaseItem(name, item) {
        this.obj[name] = this.obj[name].map(eachItem => {
            if (eachItem.itemName === item) {
                eachItem.purchased = true
                return eachItem
            } else {
                return eachItem
            }
        })
    }

    returnItem(name, item) {
        this.obj[name] = this.obj[name].filter(eachItem => {
            if (eachItem.itemName === item) {
                eachItem.purchased = false
                return eachItem
            } else {
                return eachItem
            }
        })
    }
}

const createForm = (names) => {
    const form = document.createElement("form");

    const dropdown = document.createElement("select");
    dropdown.classList.add("dropdown");
    form.appendChild(dropdown);

    names.forEach((name) => {
        let option = document.createElement("option");
        option.textContent = name;
        option.classList.add("dropdown-option");
        dropdown.appendChild(option);
    })

    const input = document.createElement("input");
    input.setAttribute("type", "text")
    input.setAttribute("placeholder", "Add Item here...")
    input.classList.add("input-element");
    form.appendChild(input);

    const AddToCartbutton = document.createElement("button");
    AddToCartbutton.setAttribute("type", "submit");
    AddToCartbutton.textContent = `Add to Cart`;
    AddToCartbutton.classList.add("add-btn")
    form.appendChild(AddToCartbutton);

    return form
}


const createCard = (heading, name, btnText, purchaseStatus) => {
    const div = document.createElement("div");
    div.classList.add("card-container");

    const cartHeading = document.createElement("h1");
    const splitName = heading.split(" ")
    cartHeading.classList.add("heading")
    cartHeading.textContent = splitName[0] + " "
    const spanHeading = document.createElement("span")
    spanHeading.textContent = splitName[1]
    spanHeading.classList.add("span-heading");
    cartHeading.appendChild(spanHeading)
    div.appendChild(cartHeading)

    const ul = document.createElement("ul");
    ul.classList.add("list-container")
    div.appendChild(ul);

    const items = customersCarts.obj[name]
    items.forEach((item) => {
        if (item.purchased === purchaseStatus) {
            const li = document.createElement("li")
            li.classList.add("list-item")

            const span = document.createElement("span")

            const checkbox = document.createElement("input")
            checkbox.setAttribute("type", "checkbox")
            checkbox.classList.add("checkbox")
            span.appendChild(checkbox)

            const labelText = document.createElement("label")
            labelText.textContent = item.itemName
            labelText.classList.add("label-text");
            span.appendChild(labelText)

            const button = document.createElement("button")
            button.textContent = "Remove"
            button.classList.add("remove-button")

            li.appendChild(span);
            li.appendChild(button);

            ul.appendChild(li);

        }
    })

    const orderButton = document.createElement("button")
    orderButton.textContent = btnText
    orderButton.classList.add("order-button")
    div.appendChild(orderButton)



    div.addEventListener("click", (event) => {
        if (event.target.tagName === "BUTTON") {
            const item = (event.target.parentNode.childNodes[0].textContent)
            if (event.target.textContent === "Remove") {
                customersCarts.removeItemFromCart(dropdown.value, item)
                event.target.parentNode.remove()
            }
            if (event.target.textContent === "Order") {
                const li = event.target.parentNode.childNodes[1].childNodes
                li.forEach(eachLi => {
                    const checked = eachLi.childNodes[0].childNodes[0].checked
                    const item = eachLi.childNodes[0].childNodes[1].textContent
                    if (checked) {
                        customersCarts.purchaseItem(dropdown.value, item)
                        clearBody()
                        renderCards(dropdown.value)
                    }
                })
            }
            if (event.target.textContent === "Return Item") {
                const li = event.target.parentNode.childNodes[1].childNodes
                li.forEach(eachLi => {
                    const checked = eachLi.childNodes[0].childNodes[0].checked
                    const item = eachLi.childNodes[0].childNodes[1].textContent
                    if (checked) {
                        customersCarts.returnItem(dropdown.value, item)
                        clearBody()
                        renderCards(dropdown.value)
                    }
                })
            }
        }
        if (event.target.tagName === "INPUT") {
            if(event.target.checked){
                const label = (event.target.parentNode.childNodes[1])
                label.style.textDecoration = "line-through"
            }
        }
    })

    return div
}

const shoppingCartContainer = (customer) => {
    return createCard("Shopping Cart", customer, "Order", false)
}

const purchasedItemsContainer = (customer) => {
    return createCard("Purchased Items", customer, "Return Item", true)
}

const renderCards = (customer) => {
    const card1 = shoppingCartContainer(customer)
    const card2 = purchasedItemsContainer(customer)

    const newDiv = document.createElement("div")
    newDiv.classList.add("cards-container")
    newDiv.appendChild(card1)
    newDiv.appendChild(card2)

    body.appendChild(newDiv)
}

const clearBody = () => {
    document.getElementsByClassName("cards-container")[0].remove()
}

const customerNames = ["Ben", "Billie", "Bob", "Boo"]
const customersCarts = new customers(customerNames)

const body = document.getElementsByTagName("body")[0];
body.classList.add("body-container")

const form = createForm(customerNames)
body.appendChild(form)

const dropdown = form.childNodes[0]
renderCards(dropdown.value)

const cardsContainer = document.getElementsByClassName("cards-container")[0]


dropdown.addEventListener("change", (event) => {
    clearBody()
    const input = document.getElementsByTagName("input")[0]
    input.value = ""
    renderCards(event.target.value)
})

form.addEventListener("submit", (event) => {
    event.preventDefault()
    const input = document.getElementsByTagName("input")[0]
    if (input.value !== "") {
        const dropdown = form.childNodes[0].value
        customersCarts.addItemToCart(dropdown, input.value)
        input.value = ""
        clearBody()
        renderCards(dropdown)
    }

})